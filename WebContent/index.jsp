<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Make call</title>
</head>
<body>
<h2>Make outbound call</h2>
<form action="MakeOutboundCall">
<p>Number: <input type="text" name="phone_no"/></p>
<input type="submit" value="Submit"/>
</form>
<%
String status = request.getParameter("status");
if (status != null) { %>
<p>Previous request: <%= status %></p>
<% } %>
</body>
</html>