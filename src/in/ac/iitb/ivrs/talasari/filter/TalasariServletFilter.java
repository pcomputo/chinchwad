package in.ac.iitb.ivrs.talasari.filter;

import in.ac.iitb.ivrs.talasari.model.EntityManagerService;

import java.io.IOException;

import javax.persistence.EntityTransaction;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation for this application that ensures that every request is associated with a database
 * transaction.
 */
@WebFilter("/*")
public class TalasariServletFilter implements Filter {

    /**
     * Default constructor. 
     */
    public TalasariServletFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * Makes sure that every request is associated with a database transaction, which is committed if everything goes
	 * well, and rolled back if anything goes wrong.
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		EntityTransaction et = EntityManagerService.getEntityManager().getTransaction();
		try {
			et.begin();
			chain.doFilter(request, response);
			et.commit();
		} catch (RuntimeException e) {

			if (EntityManagerService.getEntityManager() != null && EntityManagerService.getEntityManager().isOpen()) 
				et.rollback();
			throw e;

		} finally {
			EntityManagerService.closeEntityManager();
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
