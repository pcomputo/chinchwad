package in.ac.iitb.ivrs.talasari.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * This is a helper class that makes it easy to deal with @EntityManager's for database operations. It ensures that
 * there is a unique entity manager for each request (or thread), so that multiple parallel database operations can be
 * performed simultaneously.
 * 
 * A @EntityManagerFactory is kept open throughout the life of the application which is responsible for providing
 * @EntityManager's on demand.
 * 
 * Transaction management and @EntityManager retrieval are simplified by transparently dealing with the @EntityManager
 * corresponding to the request (or thread). 
 */
public class EntityManagerService {

	private static final EntityManagerFactory emf; 
	private static final ThreadLocal<EntityManager> threadLocal;

	static {
		emf = Persistence.createEntityManagerFactory("Chinchwadi");
		threadLocal = new ThreadLocal<EntityManager>();
	}

	/**
	 * Get the entity manager for this request (or thread) to perform persistence operations.
	 * @return The entity manager for the calling request/thread.
	 */
	public static EntityManager getEntityManager() {
		EntityManager em = threadLocal.get();

		if (em == null) {
			em = emf.createEntityManager();
			threadLocal.set(em);
		}
		return em;
	}

	/**
	 * Closes the entity manager for the calling request (or thread).
	 */
	public static void closeEntityManager() {
		EntityManager em = threadLocal.get();
		if (em != null) {
			em.close();
			threadLocal.set(null);
		}
	}

	/**
	 * Closes the entity manager factory. This is called when the application is terminating and no more persistence
	 * operations are necessary.
	 */
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	/** Prevent instantiation */
	private EntityManagerService() {
	}
}