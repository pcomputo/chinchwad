package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the message database table.
 * 
 */
@Entity
@Table(name="message")
@NamedQueries({
	@NamedQuery(name="Message.findAll", query="SELECT m FROM Message m"),
	@NamedQuery(name="Message.findMostRecent", query="SELECT m FROM Message m ORDER BY m.messageId DESC")
})
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="message_id")
	private int messageId;

	private int downloaded;

	@Column(name="message_duration")
	private int messageDuration;

	@Column(name="message_url")
	private String messageUrl;

	private int published;

	//bi-directional many-to-one association to Favorite
	@OneToMany(mappedBy="message")
	private List<Favorite> favorites;

	//bi-directional many-to-one association to Listened
	@OneToMany(mappedBy="message")
	private List<Listened> listenings;

	//bi-directional many-to-one association to Call
	@ManyToOne
	private Call call;

	//bi-directional many-to-many association to User
	@ManyToMany
	@JoinTable(
		name="favorite"
		, joinColumns={
			@JoinColumn(name="message_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="user_phone_no")
			}
		)
	private List<User> usersFavorited;

	//bi-directional many-to-one association to MessageGender
	@OneToMany(mappedBy="message")
	private List<MessageGender> messageGenders;

	//bi-directional many-to-one association to MessageLocation
	@OneToMany(mappedBy="message")
	private List<MessageLocation> messageLocations;

	//bi-directional many-to-one association to MessageTag
	@OneToMany(mappedBy="message")
	private List<MessageTag> messageTags;

	//bi-directional many-to-one association to MessageType
	@OneToMany(mappedBy="message")
	private List<MessageType> messageTypes;

	//bi-directional many-to-one association to ModeratorAssignment
	@OneToMany(mappedBy="message")
	private List<ModeratorAssignment> moderatorAssignments;

	public Message() {
	}

	public int getMessageId() {
		return this.messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getDownloaded() {
		return this.downloaded;
	}

	public void setDownloaded(int downloaded) {
		this.downloaded = downloaded;
	}

	public int getMessageDuration() {
		return this.messageDuration;
	}

	public void setMessageDuration(int messageDuration) {
		this.messageDuration = messageDuration;
	}

	public String getMessageUrl() {
		return this.messageUrl;
	}

	public void setMessageUrl(String messageUrl) {
		this.messageUrl = messageUrl;
	}

	public int getPublished() {
		return this.published;
	}

	public void setPublished(int published) {
		this.published = published;
	}

	public List<Favorite> getFavorites() {
		return this.favorites;
	}

	public void setFavorites(List<Favorite> favorites) {
		this.favorites = favorites;
	}

	public Favorite addFavorite(Favorite favorite) {
		getFavorites().add(favorite);
		favorite.setMessage(this);

		return favorite;
	}

	public Favorite removeFavorite(Favorite favorite) {
		getFavorites().remove(favorite);
		favorite.setMessage(null);

		return favorite;
	}

	public List<Listened> getListenings() {
		return this.listenings;
	}

	public void setListenings(List<Listened> listenings) {
		this.listenings = listenings;
	}

	public Listened addListening(Listened listening) {
		getListenings().add(listening);
		listening.setMessage(this);

		return listening;
	}

	public Listened removeListening(Listened listening) {
		getListenings().remove(listening);
		listening.setMessage(null);

		return listening;
	}

	public Call getCall() {
		return this.call;
	}

	public void setCall(Call call) {
		this.call = call;
	}

	public List<User> getUsersFavorited() {
		return this.usersFavorited;
	}

	public void setUsersFavorited(List<User> usersFavorited) {
		this.usersFavorited = usersFavorited;
	}

	public List<MessageGender> getMessageGenders() {
		return this.messageGenders;
	}

	public void setMessageGenders(List<MessageGender> messageGenders) {
		this.messageGenders = messageGenders;
	}

	public MessageGender addMessageGender(MessageGender messageGender) {
		getMessageGenders().add(messageGender);
		messageGender.setMessage(this);

		return messageGender;
	}

	public MessageGender removeMessageGender(MessageGender messageGender) {
		getMessageGenders().remove(messageGender);
		messageGender.setMessage(null);

		return messageGender;
	}

	public List<MessageLocation> getMessageLocations() {
		return this.messageLocations;
	}

	public void setMessageLocations(List<MessageLocation> messageLocations) {
		this.messageLocations = messageLocations;
	}

	public MessageLocation addMessageLocation(MessageLocation messageLocation) {
		getMessageLocations().add(messageLocation);
		messageLocation.setMessage(this);

		return messageLocation;
	}

	public MessageLocation removeMessageLocation(MessageLocation messageLocation) {
		getMessageLocations().remove(messageLocation);
		messageLocation.setMessage(null);

		return messageLocation;
	}

	public List<MessageTag> getMessageTags() {
		return this.messageTags;
	}

	public void setMessageTags(List<MessageTag> messageTags) {
		this.messageTags = messageTags;
	}

	public MessageTag addMessageTag(MessageTag messageTag) {
		getMessageTags().add(messageTag);
		messageTag.setMessage(this);

		return messageTag;
	}

	public MessageTag removeMessageTag(MessageTag messageTag) {
		getMessageTags().remove(messageTag);
		messageTag.setMessage(null);

		return messageTag;
	}

	public List<MessageType> getMessageTypes() {
		return this.messageTypes;
	}

	public void setMessageTypes(List<MessageType> messageTypes) {
		this.messageTypes = messageTypes;
	}

	public MessageType addMessageType(MessageType messageType) {
		getMessageTypes().add(messageType);
		messageType.setMessage(this);

		return messageType;
	}

	public MessageType removeMessageType(MessageType messageType) {
		getMessageTypes().remove(messageType);
		messageType.setMessage(null);

		return messageType;
	}

	public List<ModeratorAssignment> getModeratorAssignments() {
		return this.moderatorAssignments;
	}

	public void setModeratorAssignments(List<ModeratorAssignment> moderatorAssignments) {
		this.moderatorAssignments = moderatorAssignments;
	}

	public ModeratorAssignment addModeratorAssignment(ModeratorAssignment moderatorAssignment) {
		getModeratorAssignments().add(moderatorAssignment);
		moderatorAssignment.setMessage(this);

		return moderatorAssignment;
	}

	public ModeratorAssignment removeModeratorAssignment(ModeratorAssignment moderatorAssignment) {
		getModeratorAssignments().remove(moderatorAssignment);
		moderatorAssignment.setMessage(null);

		return moderatorAssignment;
	}

}