package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the message_gender database table.
 * 
 */
@Entity
@Table(name="message_gender")
@NamedQuery(name="MessageGender.findAll", query="SELECT m FROM MessageGender m")
public class MessageGender implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="message_gender_id")
	private int messageGenderId;

	//bi-directional many-to-one association to FixedMessageGender
	@ManyToOne
	@JoinColumn(name="gender")
	private FixedMessageGender fixedMessageGender;

	//bi-directional many-to-one association to Message
	@ManyToOne
	@JoinColumn(name="message_id")
	private Message message;

	//uni-directional many-to-one association to Moderator
	@ManyToOne
	@JoinColumn(name="moderator_id")
	private Moderator moderator;

	public MessageGender() {
	}

	public int getMessageGenderId() {
		return this.messageGenderId;
	}

	public void setMessageGenderId(int messageGenderId) {
		this.messageGenderId = messageGenderId;
	}

	public FixedMessageGender getFixedMessageGender() {
		return this.fixedMessageGender;
	}

	public void setFixedMessageGender(FixedMessageGender fixedMessageGender) {
		this.fixedMessageGender = fixedMessageGender;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Moderator getModerator() {
		return this.moderator;
	}

	public void setModerator(Moderator moderator) {
		this.moderator = moderator;
	}

}