package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the call database table.
 * 
 */
@Entity
@Table(name="call")
@NamedQuery(name="Call.findAll", query="SELECT c FROM Call c")
public class Call implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="session_id")
	private String sessionId;

	@Column(name="call_duration")
	private int callDuration;

	@Column(name="call_time")
	private Timestamp callTime;

	@Column(name="ivr_phone_no")
	private String ivrPhoneNo;

	//bi-directional many-to-one association to User
	@ManyToOne
	private User user;

	//bi-directional many-to-one association to Listened
	@OneToMany(mappedBy="call")
	private List<Listened> listenings;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="call")
	private List<Message> messages;

	public Call() {
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getCallDuration() {
		return this.callDuration;
	}

	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}

	public Timestamp getCallTime() {
		return this.callTime;
	}

	public void setCallTime(Timestamp callTime) {
		this.callTime = callTime;
	}

	public String getIvrPhoneNo() {
		return this.ivrPhoneNo;
	}

	public void setIvrPhoneNo(String ivrPhoneNo) {
		this.ivrPhoneNo = ivrPhoneNo;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Listened> getListenings() {
		return this.listenings;
	}

	public void setListenings(List<Listened> listenings) {
		this.listenings = listenings;
	}

	public Listened addListening(Listened listening) {
		getListenings().add(listening);
		listening.setCall(this);

		return listening;
	}

	public Listened removeListening(Listened listening) {
		getListenings().remove(listening);
		listening.setCall(null);

		return listening;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setCall(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setCall(null);

		return message;
	}

}