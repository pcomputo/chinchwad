package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the fixed_message_tag database table.
 * 
 */
@Entity
@Table(name="fixed_message_tag")
@NamedQuery(name="FixedMessageTag.findAll", query="SELECT f FROM FixedMessageTag f")
public class FixedMessageTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String tag;

	@Column(name="image_url")
	private String imageUrl;

	public FixedMessageTag() {
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}