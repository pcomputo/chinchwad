package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the fixed_message_type database table.
 * 
 */
@Entity
@Table(name="fixed_message_type")
@NamedQuery(name="FixedMessageType.findAll", query="SELECT f FROM FixedMessageType f")
public class FixedMessageType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="message_type")
	private String messageType;

	//bi-directional many-to-one association to MessageType
	@OneToMany(mappedBy="fixedMessageType")
	private List<MessageType> messageTypes;

	public FixedMessageType() {
	}

	public String getMessageType() {
		return this.messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public List<MessageType> getMessageTypes() {
		return this.messageTypes;
	}

	public void setMessageTypes(List<MessageType> messageTypes) {
		this.messageTypes = messageTypes;
	}

	public MessageType addMessageType(MessageType messageType) {
		getMessageTypes().add(messageType);
		messageType.setFixedMessageType(this);

		return messageType;
	}

	public MessageType removeMessageType(MessageType messageType) {
		getMessageTypes().remove(messageType);
		messageType.setFixedMessageType(null);

		return messageType;
	}

}