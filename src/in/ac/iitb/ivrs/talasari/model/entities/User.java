package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name="user")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="phone_no")
	private String phoneNo;

	private int blocked;

	@Temporal(TemporalType.DATE)
	@Column(name="blocked_till")
	private Date blockedTill;

	private String circle;

	private String operator;

	//bi-directional many-to-one association to Admin
	@OneToMany(mappedBy="user")
	private List<Admin> admins;

	//bi-directional many-to-one association to Call
	@OneToMany(mappedBy="user")
	private List<Call> calls;

	//bi-directional many-to-one association to Favorite
	@OneToMany(mappedBy="user")
	private List<Favorite> favorites;

	//bi-directional many-to-many association to Message
	@ManyToMany(mappedBy="usersFavorited")
	private List<Message> messagesFavorited;

	//bi-directional many-to-one association to Moderator
	@OneToMany(mappedBy="user")
	private List<Moderator> moderators;

	public User() {
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public int getBlocked() {
		return this.blocked;
	}

	public void setBlocked(int blocked) {
		this.blocked = blocked;
	}

	public Date getBlockedTill() {
		return this.blockedTill;
	}

	public void setBlockedTill(Date blockedTill) {
		this.blockedTill = blockedTill;
	}

	public String getCircle() {
		return this.circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public List<Admin> getAdmins() {
		return this.admins;
	}

	public void setAdmins(List<Admin> admins) {
		this.admins = admins;
	}

	public Admin addAdmin(Admin admin) {
		getAdmins().add(admin);
		admin.setUser(this);

		return admin;
	}

	public Admin removeAdmin(Admin admin) {
		getAdmins().remove(admin);
		admin.setUser(null);

		return admin;
	}

	public List<Call> getCalls() {
		return this.calls;
	}

	public void setCalls(List<Call> calls) {
		this.calls = calls;
	}

	public Call addCall(Call call) {
		getCalls().add(call);
		call.setUser(this);

		return call;
	}

	public Call removeCall(Call call) {
		getCalls().remove(call);
		call.setUser(null);

		return call;
	}

	public List<Favorite> getFavorites() {
		return this.favorites;
	}

	public void setFavorites(List<Favorite> favorites) {
		this.favorites = favorites;
	}

	public Favorite addFavorite(Favorite favorite) {
		getFavorites().add(favorite);
		favorite.setUser(this);

		return favorite;
	}

	public Favorite removeFavorite(Favorite favorite) {
		getFavorites().remove(favorite);
		favorite.setUser(null);

		return favorite;
	}

	public List<Message> getMessagesFavorited() {
		return this.messagesFavorited;
	}

	public void setMessagesFavorited(List<Message> messagesFavorited) {
		this.messagesFavorited = messagesFavorited;
	}

	public List<Moderator> getModerators() {
		return this.moderators;
	}

	public void setModerators(List<Moderator> moderators) {
		this.moderators = moderators;
	}

	public Moderator addModerator(Moderator moderator) {
		getModerators().add(moderator);
		moderator.setUser(this);

		return moderator;
	}

	public Moderator removeModerator(Moderator moderator) {
		getModerators().remove(moderator);
		moderator.setUser(null);

		return moderator;
	}

}