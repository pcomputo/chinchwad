package in.ac.iitb.ivrs.talasari.telephony.fsm.actions;

import in.ac.iitb.ivrs.talasari.model.entities.Message;
import in.ac.iitb.ivrs.talasari.telephony.TalasariSession;
import in.ac.iitb.ivrs.telephony.base.IVRSession;
import in.ac.iitb.ivrs.telephony.base.events.RecordEvent;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Transition;
import com.continuent.tungsten.commons.patterns.fsm.TransitionFailureException;
import com.continuent.tungsten.commons.patterns.fsm.TransitionRollbackException;

/**
 * Action to perform when a new record is received.
 */
public class DoReceivedRecordAction implements Action<IVRSession> {

	@Override
	public void doAction(Event<?> event, IVRSession session, Transition<IVRSession, ?> transition, int actionType)
			throws TransitionRollbackException, TransitionFailureException {

		RecordEvent recordEvent = (RecordEvent) event;
		TalasariSession talasariSession = (TalasariSession) session;
		Message recordedMessage = new Message();

		recordedMessage.setCall(talasariSession.getCall());
		recordedMessage.setMessageDuration(recordEvent.getDuration());
		recordedMessage.setMessageUrl(recordEvent.getFileURL());

		talasariSession.setRecordedMessage(recordedMessage);
	}

}
