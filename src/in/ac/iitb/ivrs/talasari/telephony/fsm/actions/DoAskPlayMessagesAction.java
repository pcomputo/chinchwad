package in.ac.iitb.ivrs.talasari.telephony.fsm.actions;

import in.ac.iitb.ivrs.talasari.config.Configs;
import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.model.entities.Message;
import in.ac.iitb.ivrs.telephony.base.IVRSession;

import java.util.List;

import javax.persistence.EntityManager;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Transition;
import com.continuent.tungsten.commons.patterns.fsm.TransitionFailureException;
import com.continuent.tungsten.commons.patterns.fsm.TransitionRollbackException;
import com.ozonetel.kookoo.CollectDtmf;
import com.ozonetel.kookoo.Response;

/**
 * Action to play three most recent recorded messages to the user.
 */
public class DoAskPlayMessagesAction implements Action<IVRSession> {

	@Override
	public void doAction(Event<?> event, IVRSession session, Transition<IVRSession, ?> transition, int actionType)
			throws TransitionRollbackException, TransitionFailureException {

		Response response = session.getResponse();
		EntityManager em = EntityManagerService.getEntityManager();
		List<Message> messages = em.createNamedQuery("Message.findMostRecent", Message.class).setMaxResults(3).getResultList();
		boolean first = true;

		CollectDtmf cd = new CollectDtmf();
		cd.setMaxDigits(1);
		cd.setTimeOut(0);
		cd.getRoot().setAttribute("skipDTMF", "012345678*#"); // XXX: This is not documented yet and is not provided by the KooKoo Java wrapper. When it is, use that instead.

		//cd.addPlayText("When you wish to go back to the main menu, press 9. Playing messages now.", Configs.Telephony.TTS_SPEED);
		cd.addPlayAudio(Configs.Voice.VOICE_DIR + "/press9ForMainMenu.wav");
		cd.addPlayAudio(Configs.Voice.VOICE_DIR + "/playingMessages.wav");

		for (Message message : messages) {
			if (first) {
				first = false;
			} else {
				//cd.addPlayText("Playing the next message now...", Configs.Telephony.TTS_SPEED);
				cd.addPlayAudio(Configs.Voice.VOICE_DIR + "/nextMessageIs.wav");
			}
			cd.addPlayAudio(message.getMessageUrl());
		}

		//cd.addPlayText("Finished playing messages.", Configs.Telephony.TTS_SPEED);
		cd.addPlayAudio(Configs.Voice.VOICE_DIR + "/finishedPlayingMessages.wav");

		response.addCollectDtmf(cd);
	}

}
