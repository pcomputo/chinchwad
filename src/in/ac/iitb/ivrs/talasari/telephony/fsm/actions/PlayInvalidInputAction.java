package in.ac.iitb.ivrs.talasari.telephony.fsm.actions;

import in.ac.iitb.ivrs.talasari.config.Configs;
import in.ac.iitb.ivrs.telephony.base.IVRSession;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Transition;
import com.continuent.tungsten.commons.patterns.fsm.TransitionFailureException;
import com.continuent.tungsten.commons.patterns.fsm.TransitionRollbackException;
import com.ozonetel.kookoo.Response;

/**
 * Actions that tells the user to input a valid input digit.
 */
public class PlayInvalidInputAction implements Action<IVRSession> {

	@Override
	public void doAction(Event<?> event, IVRSession session, Transition<IVRSession, ?> transition, int actionType)
			throws TransitionRollbackException, TransitionFailureException {

		Response response = session.getResponse();

		//response.addPlayText("Please enter a valid input digit.", Configs.Telephony.TTS_SPEED);
		response.addPlayAudio(Configs.Voice.VOICE_DIR + "/enterValidDigit.wav");
	}

}
