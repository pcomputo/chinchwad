package in.ac.iitb.ivrs.talasari.telephony;

import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.model.entities.Call;
import in.ac.iitb.ivrs.talasari.model.entities.Message;
import in.ac.iitb.ivrs.talasari.model.entities.User;
import in.ac.iitb.ivrs.talasari.telephony.fsm.TalasariStateMachine;
import in.ac.iitb.ivrs.telephony.base.IVRSession;

import java.sql.Timestamp;

import javax.persistence.EntityManager;

import com.continuent.tungsten.commons.patterns.fsm.FiniteStateException;

/**
 * Extends IVRSession to include our application-specific session fields and logic.
 * @see Call
 */
/*
 * TODO: Add scenario test cases that can be executed that considers the entire telephony application as a black box,
 *       where the tester acts as a dummy KooKoo server.
 */
public class TalasariSession extends IVRSession {

	/**
	 * The persistence object associated with this session.
	 */
	Call call;
	/**
	 * The last recorded message in this session, if any.
	 */
	Message recordedMessage;

	/**
	 * @throws InstantiationException 
	 * @see {@link IVRSession#IVRSession(String, String, String, String, String, Class)}
	 */
	public TalasariSession(String sessionId, String userNumber, String ivrNumber, String circle, String operator)
			throws FiniteStateException, InstantiationException {

		super(sessionId, userNumber, ivrNumber, circle, operator, TalasariStateMachine.class);

		EntityManager em = EntityManagerService.getEntityManager();
		User user = em.find(User.class, userNumber);
		if (user == null) {
			user = new User();
			user.setCircle(circle);
			user.setOperator(operator);
			user.setPhoneNo(userNumber);
			em.persist(user);
		}

		call = new Call();
		call.setCallTime(new Timestamp(getStartTime().getTime()));
		call.setIvrPhoneNo(getIvrNumber());
		call.setSessionId(getSessionId());
		call.setUser(user);
		em.persist(call);
	}

	/**
	 * @see IVRSession#finish(long)
	 */
	@Override
	public void finish(long totalCallDuration) {
		super.finish(totalCallDuration);
		EntityManager em = EntityManagerService.getEntityManager();

		call.setCallDuration((int) totalCallDuration);
		em.merge(call);
	}

	/**
	 * Returns the persistence object associated with this session.
	 * @return The Call persistence object.
	 */
	public Call getCall() {
		return call;
	}

	/**
	 * Sets a recorded message for this session. 
	 * @param message The message to be set.
	 */
	public void setRecordedMessage(Message message) {
		recordedMessage = message;
	}

	/**
	 * Returns the last set recorded message for this session.
	 * @return The last set recorded message for this session.
	 */
	public Message getRecordedMessage() {
		return recordedMessage;
	}

}
