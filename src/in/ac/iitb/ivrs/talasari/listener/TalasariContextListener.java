package in.ac.iitb.ivrs.talasari.listener;

import in.ac.iitb.ivrs.talasari.config.Configs;
import in.ac.iitb.ivrs.talasari.model.EntityManagerService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class for this application to ensure that the @EntityManagerFactory is
 * properly closed when the application terminates.
 */
@WebListener
public class TalasariContextListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public TalasariContextListener() {
    }

	/**
	 * Initializes the application-specific configurations.
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent e)  {
    	Configs.init();
    }

	/**
	 * Closes the @EntityManagerFactory associated with this application.
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent e)  {
    	EntityManagerService.closeEntityManagerFactory();
    }

}
